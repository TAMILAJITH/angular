import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import {  FormGroup } from '@angular/forms';
import { StudentService } from '../student.service';
import {Observable} from 'rxjs';




@Component({
  selector: 'app-student-list',
  templateUrl:'./student-list.component.html',
  styleUrls: ['./student-list.component.css']
})
export class StudentListComponent implements OnInit {

  @Input() registrationForm:FormGroup;
  @Input() currentRowIndex;
 
@Output() student=new EventEmitter();

  get studentName() {
    return this.registrationForm.get('studentName');
  }
  get fatherName() {
    return this.registrationForm.get('fatherName');
  }
  get dateOfBirth() {
    return this.registrationForm.get('dateOfBirth');
  }
  get department() {
    return this.registrationForm.get('department')
  }
  get address() {
    return this.registrationForm.get('address')
  }
  get email() {
    return this.registrationForm.get('email')
  }
  get age() {
    return this.registrationForm.get('age')
  }
  get password() {
    return this.registrationForm.get('password')
  }
  get confirm() {
    return this.registrationForm.get('confirm')
  }
  constructor( private _StudentService: StudentService) { }

  showValue="";
value="Checking"
  ngOnInit() {
  this._StudentService.var.subscribe(x=>this.showValue=x);

  }           
  
  showSubject(){
    this._StudentService.shareData(this.value);
  }

  InsertStudent() {
    if(this.currentRowIndex==null){
      this._StudentService.storeValue(this.registrationForm.value);
    this.student.emit();
    }else{
      this.update();
      this.student.emit();
    }
    
  }

  
  open(){
  this.registrationForm.reset();
 
  }
 update(){
   var store=this.registrationForm;
   var get=store[this.currentRowIndex];
  

 }

} 