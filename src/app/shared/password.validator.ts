import {AbstractControl} from '@angular/forms'
export function passwordValidator(control :AbstractControl):{[key:string]:boolean} |null{

    const password=control.get('password');
    const confirm=control.get('confirm');
    if (password.pristine || confirm.pristine){
     return   null;
    }
  return  password && confirm && password.value !== confirm.value ? {'mismatch':true} : null;

}

export function EmailValidators(control:AbstractControl):{[key:string]:boolean} |null{
  
  let email=control.get('email.value');
return email.value.includes("@") ?{null :true} : {'emailError':false};
}