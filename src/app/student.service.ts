import { Injectable, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Subject } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class StudentService  {

  constructor() { }

var:Subject<any>=new Subject();
  studentList = [];

  shareData(data){
  this.var.next(data)
  }
  storeValue(registrationForm) {

    var store = this.getValue();
    store.push(registrationForm);
    localStorage.setItem("StudentList", JSON.stringify(store))
  }
  DeleteMethod(index) {

    var allStudent = this.getValue();
    allStudent.splice(index, 1);
    this.storeDeleteValue(allStudent);
  }
  storeDeleteValue(l) {

    localStorage.setItem('StudentList', JSON.stringify(l));
  }


  updateAllStudentToLocalStorage(ar: any[]) {
    
    localStorage.setItem("StudentList", JSON.stringify(ar))
  }

  getValue() {

    var studentList = [];
    var getValue = localStorage.getItem('StudentList');
    if (getValue) {
      studentList = JSON.parse(getValue)
    } else {
      studentList = [];
    }
    console.log(studentList)
    return studentList;

  }

  updateStudent(currentIndex: number, studentObject: object) {
    
    let students = this.getValue();
    students[currentIndex] = studentObject;
    this.updateAllStudentToLocalStorage(students);

  }
  // onOpen(registrationForm:FormGroup){
  //   registrationForm.studentName="";
  //   registrationForm.fatherName="";
  //   registrationForm.gender="";
  //   registrationForm.dateOfBirth="";
  //   registrationForm.department="";
  //   registrationForm.address="";
  //   registrationForm.email="";
  //   registrationForm.age="";
  //   registrationForm.password="";
  //   registrationForm.confirm="";
  // }



  // storeLocal(student){

  //  var store=this.getLocal();
  //    store =student; 
  //   localStorage.setItem("studentList",JSON.stringify(store))
  // }

  // getLocal(){

  //  var studentList =[];
  //  var getValue = localStorage.getItem("studentList");
  //  if(getValue){
  //   studentList=JSON.parse(getValue);
  //  }else()=>{
  //    studentList =[];
  //  }
  //  return studentList;

  // }



}
