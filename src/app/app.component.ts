import { Component, OnInit } from '@angular/core';
import { StudentService } from './student.service';
import { FormBuilder, Validators } from '@angular/forms';
import { passwordValidator,EmailValidators } from './shared/password.validator';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  constructor(private _studentService: StudentService, private fb: FormBuilder,) { }
   currentRowIndex=null;
  title = 'task-seven';

  registrationForm = this.fb.group({
    studentName: ['', [Validators.required, Validators.minLength(2)]],
    fatherName: ['', Validators.required],
    gender: ['', Validators.required],
    dateOfBirth: ['', Validators.required],
    department: ['', Validators.required],
    address: ['', Validators.required],
    email: ['', Validators.required,EmailValidators],
    age: ['', [Validators.required]],
    password: ['', Validators.required],
    confirm: ['', Validators.required,]
   }, 
   { validator: passwordValidator, }
   )

  student = [];

  ngOnInit() {

    this.student = this._studentService.getValue();
   
  }

  Delete(i) {
    this._studentService.DeleteMethod(i);
    this.student = this._studentService.getValue();
  }
  edit(s,i) {
    
    this.registrationForm.patchValue(s);
    this.currentRowIndex=i;
    
    
  }

 
  
}
